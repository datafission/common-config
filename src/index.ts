'use strict';

/**
 * Module Dependencies
 */
import * as express from 'express';
import * as path from 'path';
import {spawn} from 'child_process';

const app = express();

app.use('/src', express.static(path.resolve(__dirname, '..', 'src', 'public')));
app.use('/dist', express.static(path.resolve('public')));

/**
 * Route for git hook
 */
app.post('/git-pull', async (req, res, next) => {

    res.status(202).json({
        status: 'Request Received'
    });

    await pull();
    await compile();
});

var __appPort = process.env.PORT || 8000;

app.listen(__appPort, () => {
    console.log(`[*] Server started on port ${__appPort}`);
});

function pull() {
    return new Promise((resolve, reject) => {
        /**
         * Spawn a process for git pull
         */
        let child = spawn('git', ['pull'], {
            detached: true
        });
        child.stdout.on('data', (data) => {
            console.log(`stdout: ${data}`);
        });
        child.stderr.on('data', (data) => {
            console.log(`stderr: ${data}`);
        });
        child.on('close', (code) => {
            console.log(`child process exited with code ${code}`);
            resolve();
        });
    });
}

function compile() {
    return new Promise((resolve, reject) => {
        /**
         * Spawn a process for typescript compile
         */
        let tsc = spawn('tsc', [], {
            detached: true
        });
        tsc.stdout.on('data', (data) => {
            console.log(`tsc stdout: ${data}`);
        });
        tsc.stderr.on('data', (data) => {
            console.log(`tsc stderr: ${data}`);
        });
        tsc.on('close', (code) => {
            console.log(`tsc process exited with code ${code}`);
            resolve();
        });
    });
}
