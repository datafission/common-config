/**
 * This file contains all the config
 * required by the application
 */

/**
 * Config Object
 */
export const config = {
    mq: {
        id: 'be38f4ca-31eb-4d4b-a2ed-81edff6f2fe4',
        connectionString: 'amqp://admin:admin@52.39.124.107:5672?heartbeat=30',
        apiConnectionString: 'http://service:password@52.39.124.107:15672/api'
    },
    redis: {
        url: 'redis://52.39.124.107:6379/0'
    },
    extractor: {
        id: '2b591644-a67c-41fe-b4b2-94eb39c9409d',
        appPort: process.env.PORT || 8002,
        queueName: 'dataExtractorQueue',
        prefetchCount: 1,
        mimeBindings: {
            XSV: [
                'text/csv'
            ],
            EXCEL: [
                'application/vnd.ms-excel',
                'application/msexcel',
                'application/x-msexcel',
                'application/x-ms-excel',
                'application/x-excel',
                'application/x-dos_ms_excel',
                'application/xls',
                'application/x-xls',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            ]
        }
    },
    ganga: {
        id: '236ad6ee-de09-4390-a98a-bd8d87902554',
        appPort: process.env.PORT || 8001
    },
    godown: {
        id: 'dba18708-7281-484c-be41-9906c764c38f',
        appPort: process.env.PORT || 8000
    },
    gun: {
        id: 'c9ebe576-4857-4546-9342-7a385528e4ac',
        appPort: process.env.PORT || 8003,
        queueName: 'dataGunQueue',
        prefetchCount: 1,
        getDbConnectionString
    },
    aws: {
        id: '669f34fc-3d6c-4cb7-85ec-9c002245d2ed',
        "accessKeyId": "AKIAJXZB24I3Y55JDV4A",
        "secretAccessKey": "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",
        s3: {
            path: 'https://s3.ap-south-1.amazonaws.com',
            bucket: 'qetl'
        }
    },
    retryInterval: 1000
}

/***********************************************
 * Extractor                                   *
************************************************
*/
export function getFileType (mimeType: string): string {
    let fileType = null;
    var bindings = config.extractor.mimeBindings;

    Object.keys(bindings).forEach(function (key) {
        let list = (<any>bindings)[key];
        if (list.indexOf(mimeType) != -1) {
            fileType = key;
        }
    });
    return fileType;
}

/***********************************************
 * Extractor                                   *
************************************************
*/
function getDbConnectionString (dbName: string): string {
    return `mongodb://service:service@52.39.124.107:27017/${dbName}?ssl=false&authSource=admin`;    
}

/***********************************************
 * Process                                     *
************************************************
*/
process.on('unhandledRejection', (err) => {  
    console.error(err);
});
process.on('uncaughtException', function (err) {
    console.error(err);
});
