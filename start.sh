#!/bin/bash

has() {
  type "$1" > /dev/null 2>&1
  return $?
}

if has "git"; then
  echo "Git Present"
else
  echo "Error: you need git to proceed" >&2;
  exit 1
fi

echo "Installing npm packages"
npm install --registry "https://registry.npmjs.org"

echo "Compiling Application"
tsc

echo "Starting Application"
pm2 start dist/index.js --watch

# echo "Starting git pull loop"
# while true
# do
#   echo "Pulling Now for changes"
#   git pull
#   sleep 60
# done
